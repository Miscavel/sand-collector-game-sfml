# Sand Collector Game SFML

Sand Collector Game made in SFML-2.3.2

Project compiled using Visual Studio 2010 32-bit

The game is inspired by [Lode Runner](https://www.c64-wiki.com/wiki/Lode_Runner).

Controls :
1. Movement - Arrow Keys
2. Dig - 'Z' key
3. Restart - 'R' key
4. Quit - 'Esc' key

Gameplay :
1. Collect all the sands in the stage to unlock the door to the next level.
2. Pressing the 'Z' key will remove the tile in front of the player temporarily. Removed tiles respawn after 1 second.
3. Unlike in Lode Runner, if the player intersects with a tile when it respawns, the player does not die and instead will be pushed upwards. 
Repeat the process until the player no longer intersects with any tile.
4. The stages are designed with regards to the mechanics explained in (3).

![Gameplay Video](Media/trailer.mp4)

Credits to Sprite Creator XP for the Player Sprite.

